package service.impl;

import java.util.Collection;

import org.apache.commons.collections4.IteratorUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.essoustore.moejaa.model.User;

import dao.IRoleRepository;
import dao.IUserRepository;
import service.IUserService;

/**
 * @author Admin
 *
 */
@Service(value = "userService")
public class UserServiceImpl implements IUserService {

    @Autowired
    private IUserRepository       userRepository;
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public UserServiceImpl() {
        super();
    }

    @Autowired
    public UserServiceImpl(IUserRepository userRepository, IRoleRepository roleRepository, BCryptPasswordEncoder bCryptPasswordEncoder) {
        super();
        this.userRepository = userRepository;

        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    /**
     *
     * Permet de trouver tous les users
     */
    @Override
    public Collection<User> getAllUser() {
        return IteratorUtils.toList(userRepository.findAll().iterator());
    }

    @Override
    public User getUserById(Long id) {
        return userRepository.findById(id).orElse(null);
    }

    /**
     *
     */
    @Override
    public User findByLogin(String login) {
        return userRepository.findByLogin(login);

    }

    @Override
    @Transactional(readOnly = false)
    public User saveOrUpdateUser(User user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        return userRepository.save(user);
    }

    @Override
    @Transactional(readOnly = false)
    public void deleteUser(Long id) {
        userRepository.deleteById(id);

    }

}
