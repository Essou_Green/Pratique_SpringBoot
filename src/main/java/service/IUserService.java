package service;

import java.util.Collection;

import com.essoustore.moejaa.model.User;

/**
 * @author Admin
 *
 */
public interface IUserService {

    /**
     * @return
     */
    Collection<User> getAllUser();

    /**
     * @param id
     * @return
     */
    User getUserById(final Long id);

    /**
     * @param login
     * @return
     */
    User findByLogin(final String login);

    /**
     * @param user
     * @return
     */
    User saveOrUpdateUser(final User user);

    /**
     * @param id
     */
    void deleteUser(final Long id);

    /**
     * @param login
     * @param password
     * @return
     */
 //   Optional<User> findByLoginAndPassword(final String login, final String password);
}
