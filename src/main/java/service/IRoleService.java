package service;

import java.util.Collection;
import java.util.stream.Stream;

import com.essoustore.moejaa.model.Role;

/**
 * @author Admin
 *
 */
public interface IRoleService {

    /**
     * @param name
     * @return
     */
    Role findByRoleName(final String roleName);

    /**
     * @return
     */
    Collection<Role> getAllRoles();

    /**
     * @return
     */
    Stream<Role> getAllRolesStream();

}
