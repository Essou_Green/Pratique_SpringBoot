package dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.essoustore.moejaa.model.User;

/**
 * @author Admin
 *
 */
public interface IUserRepository extends JpaRepository<User, Long> {

    /**
     * @param login
     * @return
     */
    
    User findByLogin(final String login);
    

}
