package dao;

import java.util.stream.Stream;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.essoustore.moejaa.model.Role;

/**
 * @author Admin
 *
 */
public interface IRoleRepository extends JpaRepository<Role, Long> {

    /**
     * @param roleName
     * @return
     */
    Role findByRoleName(final String roleName);

    /**
     * @return
     */
    @Query("select role from Role role")
    Stream<Role> getAllRolesStream();

}
