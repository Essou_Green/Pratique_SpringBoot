package com.essoustore.moejaa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Moejaa1Application {

	public static void main(String[] args) {
		SpringApplication.run(Moejaa1Application.class, args);
	}

}
